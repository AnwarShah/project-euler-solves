#implementation of a virtual shell
class CustomException < StandardError
    def initialize(message=nil)
        super(message)
    end
end
class Computer
    @@users = {}
    def initialize(username, password)
        @username = username
        @password =password
        @files = {}
        @@users[username] = password
    end
    def create(filename, owner)
        time = Time.now
        if @@users.include?(owner)
            if @files.include?(filename) 
                puts "File \'#{filename}\' already exist"
            else 
        	   @files[filename] = { owner: owner, time: time }
               puts "A new file is created by #{owner} with name #{filename}"
            end
        else
            raise CustomException.new("User doesn't exist")
        end
    end
	def get_files
        @files
	end
    def delete_file(filename, owner)
        if @files.include?(filename)   #file exists
            if @files[filename][:owner] == owner
                @files.delete(filename)
            end
        end
    end
	def Computer.add_user(username, password)
		@@users[username] = password
	end    
    def Computer.get_users
        @@users
    end

end

anwar_pc = Computer.new("anwar", "anwar")
Computer.add_user("iqbal", "iqbalpass")
anwar_pc.create("groceries.txt", "anwar")
anwar_pc.create("todos.txt", "iqbal")
anwar_pc.create("todos.txt", "anwar")

puts anwar_pc.get_files


anwar_pc.delete_file("todos.txt", "iqbal")

puts anwar_pc.get_files
puts Computer.get_users