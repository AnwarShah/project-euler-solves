movies = {
    lotr: 3.4,
    up: 3,
    robocop: 1,
    batman: 2
}

puts "What do you want to do with movie database?
- add (Add movies to the database)\n
- update (Update a movie rating)\n
- display (Display the movies in the database)\n
- delete (Delete a movie from database)"

choice = gets.chomp.downcase	#get the user choice

case choice 
when "add"
	puts "Enter the title of the movie: "
	title = gets.chomp.downcase.to_sym
	puts "Enter the rating of the movie: "
	rating = gets.chomp.to_i
	# check whether movie is already there
	if movies[title].nil? then
		movies[title] = rating
		puts "Added!"
	else
		puts "The movie is already there with rating: #{movies[title]}"
	end
when "update"
	puts "Enter the movie title you want to update"
	title = gets.chomp.downcase.to_sym
	#check whether the title exists?
	if movies[title].nil? then
		puts "Error, The movie is not in the database"
	else
		puts "Enter the new rating for the movie "
		rating = gets.chomp.to_i
		movies[title] = rating
	end
when "display"
	movies.each do 
	    |movie, rating| 
	    puts "#{movie}: #{rating}"
	end
when "delete"
	puts "Which movie you want to delete? (Enter title)"
	title = gets.chomp.downcase.to_sym
	if movies[title].nil? then	#not exists?
		puts "Sorry, the movie is not found in the database"
	else
		movies.delete(title)
		puts "The movie got deleted!"
	end
else
	puts "Sorry, I didn't get you!"
end

puts movies