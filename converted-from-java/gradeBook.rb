#This is a Ruby version of the program described in Java-How to program
# 6th edition, in Fig-4.9
class GradeBook
	attr_accessor :courseName	#create methods for courseName
	
	def initialize(courseName)
		@courseName = courseName
	end

	def input_grades()
		puts "Enter the integer grades in the range 0-100\n\
type 'end' as the indicator of end of input"
		input = gets(sep='end')	#read until 'end' 
		grades = input.split()
		@total = 0; @grade_counter = 0 #create and initialize
		@a_count = @b_count = @c_count = @d_count = @f_count = 0 #init grade counts
		grades.each {
			|grade|
			begin
				num_grade = Integer(grade)
			rescue ArgumentError
				puts "skipping non integer value '#{grade}'"
			end
			if num_grade.is_a?(Integer)
				@total += num_grade
				@grade_counter += 1
				increment_letter_grade_counter(num_grade)
			end
		}
	end
	
	def display_message
		puts "Welcome to the gradebook for #{@courseName}"
	end
	
	def display_grade_report
		puts "\nGrade Report:"
		if @grade_counter != 0 #user entered at least 1
			average = (@total / @grade_counter).to_f
			printf("Total of the %d grades entered is %d: \n", @grade_counter, @total)
			printf("Class average is %.2f\n", average)
			printf("Number of students who received each grade:\n")
			printf("A: %d\n", @a_count)
			printf("B: %d\n", @b_count)
			printf("C: %d\n", @c_count)
			printf("D: %d\n", @d_count)
			printf("F: %d\n", @f_count)
		end
	end
	
	def increment_letter_grade_counter(num_grade) 
		case (num_grade/10)
		when 9,10	#between 90 to 100
			@a_count += 1
		when 8
			@b_count += 1
		when 7
			@c_count += 1
		when 6
			@d_count += 1
		else
			@f_count += 1
		end
	end

	private :increment_letter_grade_counter
end


grbook = GradeBook.new("Introduction to Ruby")
grbook.display_message
grbook.input_grades
grbook.display_grade_report
