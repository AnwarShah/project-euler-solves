class Account(object):
	def __init__(self, init_balance):
		if init_balance > 0.0:
			self.__balance = init_balance
		else:
			self.__balance = 0.0
	
	def credit(self, amount):
		self.__balance += amount

	def get_balance(self):
		return self.__balance

account1 = Account(50.0)
account2 = Account(-7.53)

print("Account balance for account1: %.2f" % (account1.get_balance()))
print("Account balance for account2: %.2f" % (account2.get_balance()))

dep_amount = input("Enter deposit amount for account1: ")
account1.credit(float(dep_amount))

print("Account balance for account1: %.2f" % (account1.get_balance()))
print("Account balance for account2: %.2f" % (account2.get_balance()))

dep_amount = input("Enter deposit amount for account2: ")
account2.credit(float(dep_amount))

print("Account balance for account1: %.2f" % (account1.get_balance()))
print("Account balance for account2: %.2f" % (account2.get_balance()))
