#This is a Java-to-Ruby conversion of an example from Deitel's Java book.
# Example Fig 3.14 from Java- How to Program Sixth edition
class Account
	attr_reader :balance
	def initialize(init_amount=0.0)
		@balance = init_amount > 0.0 ? init_amount : 0.0
	end
	def credit=(amount)
		@balance += amount
	end
end

account1 = Account.new(50.0)
account2 = Account.new(-7.53)

puts "account1 balance: $#{account1.balance}"
puts "account2 balance: $#{account2.balance}"

print "Enter deposit amount for account1: "
dep_amount = gets.chomp.to_f
account1.credit = dep_amount

puts "account1 balance: $#{account1.balance}"
puts "account2 balance: $#{account2.balance}"

print "Enter deposit amount for account2: "
dep_amount = gets.chomp.to_f
account2.credit = dep_amount

puts "account1 balance: $#{account1.balance}"
puts "account2 balance: $#{account2.balance}"
