#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    
    int trig[100][100] = { 0 } ;
    int sum[100][100] = { 0 };
    int k, max = 0 ;
    
    int cases, rows, cols = 0;
    int row, col = 0; // counter
    int upsum, upleftsum = 0;
    
    scanf("%d", &cases);
    
    while( cases-- )
    {
        scanf("%d", &rows);
        row = 0;
        while(row < rows)
        {
            cols = row;
            for(col=0; col<=cols; col++)
            {
                scanf("%d", &trig[row][col]);
            }
            
            // fill the sums triangle
            if ( row == 0 )
            {
                sum[row][0] = trig[row][0];
            }
            else // row > 0
            {
                for(col=0; col <=cols; col++)
                {
                    if (cols == 0 )
                    {
                        sum[row][0] = trig[row][0] + sum[row-1][0];
                    }
                    else
                    {
                        upsum = sum[row-1][col] + trig[row][col];
                        upleftsum = sum[row-1][col-1] + trig[row][col];
                        sum[row][col] = upsum > upleftsum ? upsum : upleftsum;
                    }
                }
            }
            row++;
        } // end of a case
        
        max = sum[rows-1][0];
        for(k=1; k<rows; k++)
        {
            max = sum[rows-1][k] > max ? sum[rows-1][k] : max;
        }
        printf("%d\n", max);
    }
    return 0;
}

