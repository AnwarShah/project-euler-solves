/* This program will count the available digits in a given text */

// digits object will hold the count for each digits
var digits = {
	0 : 0,
	1 : 0,
	2 : 0,
	3 : 0,
	4 : 0,
	5 : 0,
	6 : 0,
	7 : 0,
	8 : 0,
	9 : 0
}

/* this function takes a single character and test and increse digit count*/
var increaseDigitCount = function(x){
	switch(x){
		case "0":
			digits[x]++;
			break;
		case "1": 
			digits[x]++;
			break;
		case "2":
			digits[x]++;
			break;
		case "3":
			digits[x]++;
			break;
		case "4":
			digits[x]++;
			break;
		case "5":
			digits[x]++;
			break;
		case "6":
			digits[x]++;
			break;
		case "7":
			digits[x]++;
			break;
		case "8":
			digits[x]++;
			break;
		case "9":
			digits[x]++;
			break;
		default:
			//console.log("Not a digit");
	}
}

/*
//test `digits`

for(var i=1; i <= 100; i++){
	for(key in digits){
		digits[key]++;	// increase the value
	}
}
// Awesome!! it works!!
*/

var text = "DAILY NEWSNew YorkNewsPoliticsSportsEntertainmentOpinionLivingAutos U.S. 
WORLD CRIME THE WEEK NEWS PICS BLOGS Uber’s lost and found records for 155 items leaked on public website,
 company says it was an accident The ride-share company appeared to have accidentally made the
  lost and found records for customers in Southern California public for an undetermined period of time. The records also showed
   the phone numbers of some customers and revealed that two drivers requested money for returned items. BY JASON SILVERSTEIN  NEW
    YORK DAILY NEWS Monday, February 9, 2015, 10:56 PM A A A 1 8 SHARE THIS URL  MOTHERBOARD/VICE
     Uber left some of its lost and found records, including customer phone numbers, public for an undetermined period of
      time. Add this to Uber’s lost-and-found for the day: customer privacy.  Uber made some of its lost and found records public, apparently by
       accident, exposing not only the forgotten possessions but also the names and phone numbers for dozens of customers.  The records appeared to be from the ride-share company’s Los Angeles office, since most of the exposed phone numbers have Southern California area codes, according to Motherboard.  The webpage with the records disappeared within two hours of the story breaking. It’s unclear how long it was public.  MOTHERBOARD/VICE The database disappeared within two hours of the news breaking. The page showed 155 items dating back to early December, including wallets, credit cards and iPhones, along with phone numbers and internal identification numbers for rides. Some customers were logged multiple times for several items.  These are some of the most unusual items that were logged, according to Motherboard:  Santa hat Spanish vinyl records  $500 cash Blue portable seat Crisco oil and Ice Breakers Mint Medical weed cards Selfie stick Bag of stuff The records showed that two drivers requested money in exchange for returning the items.  ERIC RISBERG/AP Uber has had several privacy breaches in the past few months and recently initiated an audit of its security practices. Uber said in a statement to the Daily News: It appears that this log of lost items was accidentally made public, and we’re sorry for this mistake.  We are looking into exactly how this happened so that it does not happen again.  In most of the 54 countries where Uber operates, customers can report lost items directly to their driver using the app, according to the company’s website.  Uber has had several larger privacy breaches in the past few months, including the revelation that its executives proposed digging up embarrassing information about journalists who write critically about the multibillion dollar company. It recently hired the former chief privacy officer of IBM to audit the company’s security practices.  Follow @jaysunsilver  With Alejandro Alba  jsilverstein@nydailynews.com  MORE FROM NYDAILYNEWS 15 vehicles crash on N.J. turnpike, at least 1 dead Jesse Matthew hit with murder charge in Hannah Graham case Recommended by COMMENTSPOST A COMMENT[ Discussion Guidelines] SORT EDITORS' PICKS MOUNTAIN MYSTERY SOLVED: Remains of vanished Chilean soccer team plane found high in Andes after 54 years — SEE THE VIDEO Mountaineers made a starting discovery while hiking the Andes over the weekend: the remains of a plane that disappeared 54 years ago. Woman destroyed husband's car after learning of affair: cops A woman in South Carolina allegedly took her husband’s gun, then destroyed his car, after learning that he was cheating on her. Ohio mom accused of aiding boyfriend in rape... A little girl’s brave escape to her neighbors’ home ended in the arrest of her mother and her mother’s boyfriend for alleged rape. Uma is that you? Thurman unrecognizable on red carpet Uma Thurman looked nearly unrecognizable on the r red carpet at the premiere party of her new NBC miniseries The Slap Monday. ISIS hostage Kayla Jean Mueller confirmed dea... Kayla Mueller, the Arizona aid worker held hostage by the Islamic State has died, her family said Tuesday. Brooklyn suit: Murdered woman’s casket popped open... A Brooklyn native killed by her husband was allegedly violated anew when her casket didn’t fit the grave, popped open and filled with soil. IVY LEAGUER IN SHOTGUN HORROR: Cornell student charged with gunning down his CEO father at family's home A Cornell University student gunned down his CEO father with a shotgun at the family’s home in a wealthy Rochester suburb, authorities said. SEE VIDEO Texas woman headbutts tax worker in Walmart brawl A ticked-off customer at a Texas Walmart headbutted a Jackson Hewitt employee who told her to take her 'poor a-- out of this store.' EXCLUSIVE: Oregon webcam girl Kendra Sunderland, who took the Internet by storm with library porn adventure, opens up in first interview — 'I’ve always been the type of girl who’s not afraid to show off' The former Oregon State student who went viral with a NSFW video from the school library shares her secrets. Delta Airlines apologizes for X-rated Facebook hack Hackers took over Delta Airlines’ Facebook page and uploaded embarassing, X-rated posts on Tuesday, the airline said. Virginia man planned to have sex with 5-year-old girl at motel, brought a teddy bear and sex toy: cops Man carrying teddy bear, sex toy, marijuana arrested after showing up to motel believing he'd have sex with 5-year-old girl. San Diego sports anchor shot in 'targeted' attack: cops A San Diego sports anchor was in surgery Monday night as police hunted an “armed and dangerous” gunman who shot him down in his driveway. BEFORE 'FIFTY': Movies that explore sexual taboos Long before '50 shades of Grey' came out, controversial films since the mid-1900s started exploring sexual taboos from BDSM, child prostitution and even incest. Ohio infant dies from mug thrown during parents' argument A 2-month-old infant died Tuesday after getting hit in the head with a mug that a Canton, OH man threw during a domestic dispute. Rockland County principal berated and bullied special ed students, suit alleges A special ed principal was caught on tape berating a student, calling him a retard before a large school assembly. TRAGIC DATE: Family will end Bobbi Kristina’s life support TOMORROW on grim anniversary of Whitney Houston’s death The tragic saga of Whitney Houston's only daughter will apparently come to an end on the third anniversary of the singer's own death. 'I don’t think it makes any sense... everything is frozen solid': Alternate-side-of-the-street parking resumes, angering drivers as they dig cars out of snow The resumption of alternate-side-of-the-street parking caused problems for drivers who cars buried in snow. Lil Kim debuts much tighter face, sparks more rumors about plastic surgery Some confessed, others denied - vehemently. See which stars have (allegedly) upgraded their physical assets, and which say they haven't. EXCLUSIVE 150 tour guides at Gray Line bus company will be laid off The operator of the city’s ubiquitous Gray Line double-decker tour buses is planning “mass layoffs” March 11. Twitter reacts to Brian Williams' unpaid suspension The news that NBC has suspended disgraced anchor Brian Williams without pay for six months brought the trending Twitter talk to a crescendo. CONDEMNED: Dozens of NYC building inspectors and contractors surrender to authorities over massive bribery scam File this criminal scheme under “condemned.” Rosie O'Donnell will make an earlier exit from 'The View' Rosie O’Donnell’s going out the only way she knows how: on her own terms. Pa. doctor sexually assaulted 12 female patients after getting them hooked on drugs: police Jay J. Cho, 71, is accused of hooking the women to narcotics before threatening to stop treating them unless they gave in to his demands. MOST POPULAR MOST READ MOST SHARED 1 San Diego sports anchor shot in 'targeted' attack: cops 2 Kayla Jean Mueller was forced to marry ISIS leader: report 3 Cornell student charged in shotgun murder of CEO dad 4 Oregon webcam girl Kendra Sunderland gives first interview 5 New tech adviser for Jeb Bush's potential campaign resignsm, 6 SEE IT! Texas woman headbutts tax worker in Walmart brawl 7 Delta Airlines apologizes for X-rated Facebook hack 8 Woman destroyed husband's car after learning of affair: cops 9 Doc hooked patients on drugs, sexually assaulted them: cops 10 Zuri Whitehead's says accused killer asked to watch baby BEST CITY APP IN THE WORLD!  App StoreAndroid FREE NEWS VIDEO  WH: At least one other U.S. hostage being held Washington Examiner NASA video captures moment sunrise meets aurora borealis ITN NYPD officer indicted in fatal shooting of unarmed man: WSJ Reuters First Family Targeted in Apparent ISIS Twitter Hack Inside Edition French trial over 271 'stolen' Picasso works AFP 'Serial Stowaway' Arrested Again in Florida CBS Minnesota WH: At least one other U.S. hostage being held Washington Examiner NASA video captures moment sunrise meets aurora borealis ITN More videos: French trial over 271 'stolen' Picasso works 'Serial Stowaway' Arrested Again in Florida WH: At least one other U.S. hostage being held NASA video captures moment sunrise meets aurora borealis NYPD officer indicted in fatal shooting of unarmed man: WSJ First Family Targeted in Apparent ISIS Twitter Hack French trial over 271 'stolen' Picasso works 'Serial Stowaway' Arrested Again in Florida WH: At least one other U.S. hostage being held NASA video captures moment sunrise meets aurora borealis NYPD officer indicted in fatal shooting of unarmed man: WSJ First Family Targeted in Apparent ISIS Twitter Hack PC Richard and Son $ 498.97 Sony 48 Class LED 1080p HDTV with Built-In WiFi $ 799.96 Sharp 65 Class LED HDTV $ 218.97 Samsung 32 Class LED HDTV $ 398.97 Sony 40 Class LED 1080p HDTV with Built-In WiFi Media Kit Home Delivery Newsletters Businesses Place an Ad About our Ads Contact Us Careers FAQ's Feeds Site Map Use of this website signifies your agreement to the Terms of Service and Privacy Policy. © Copyright 2015 NYDailyNews.com. All rights reserved. "

//var text = "this is my new phone number. 01722686936";

for(var i=0; i < text.length; i++){
	increaseDigitCount(text[i]);
}

console.log(digits);