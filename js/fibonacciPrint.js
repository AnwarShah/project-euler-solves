// This function computes fibonacci numbers

function fibonacci(num){
	var f1 = 1;
	var f2 = 2;
	var f3 = f2 + f1;
	
	//first print the two	
	document.getElementById("result").innerHTML += (1 + " = " + f1 + " <br/>");
	document.getElementById("result").innerHTML += (2 + " = " + f2 + " <br/>");

	for(var i=3; i<=num; i++){
			f3 = f2 + f1;
			f1 = f2; // the current f2 will be f1 of the next round
			f2 = f3; // current f3 will become f2 on the next round
			document.getElementById("result").innerHTML += (i + " = " + f3 + " <br/>");
	}

}
