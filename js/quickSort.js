function quickSort(A, p, r){
	var q;
	if(p < r){
		q = partition(A, p, r);
		quickSort(A, p, q-1);
		quickSort(A, q+1, r);
	}
}

function partition(A, p, r){
	var x = A[r];	// pivot is the rightmost element
	var i = p - 1;	// lower elements' array counter
	var j = p;		// higher elements' array counter

	for(j; j<r; j++){	//until rightest-1 element
		if(A[j] <= x){
			i = i+1;
			exchange(A, i, j); // exchange the elements in index j, i
								// in Array A
		}
	}
	exchange(A, i+1, r);

	return i+1;
}

// this function get to element of an array and exchange there position
function exchange(A, a, b){
	var temp = A[a];
	A[a] = A[b];
	A[b] = temp;
}

var A = [2, 3, 9, 4, 90, 33];

quickSort(A, 0, A.length-1)

//document.write(A) ;
console.log(A);