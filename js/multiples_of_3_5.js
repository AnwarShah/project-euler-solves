/* problem url: https://projecteuler.net/problem=1 */
function multipleThreeFive(){
	var sum = 0;
	var num = 1;

	while(num < 1000){
		if(num%3 ===0 || num%5===0){
			sum += num;
		}
		num++;
	}	
	// document.getElementById("result").innerHTML = sum;
	console.log(sum);
}

multipleThreeFive();