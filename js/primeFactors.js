/* Problem url: https://projecteuler.net/problem=3
/* This program will output the largest factor of a number, which is prime */

// implementation conversion
/* Based on AKS primality test. 
Source help: http://stackoverflow.com/a/1801446/1039893 */
var isPrime = function(number){
	if(number === 2) 
		return true;
	if(number === 3)
		return true;
	if(number%2 === 0) 
		return false;
	if(number%3 === 0)
		return false;

	var i = 5;
	var w = 2;
	while (i*i <= number){
		if(number%i === 0)
			return false;

		i += w;
		w = 6-w;		
	}
	return true;
}


var printPrimeFactors = function(number){
	var factor = 2; 	// starting with the lowest prime factor other than 1
	var quotient = number;	// first quotient is the number itself
	//repeat until quotient is also prime
	while(true){
		// if the number is divisible by current prime factor
		if(quotient%factor === 0){
			//divide the number
			quotient = quotient/factor;

			// increment the factor count. 	
			// store the factor. that is print that
			console.log(factor);

			//Check the quotient. 
			//if quotient itself is prime, that means our job is done. 
			// break;
			if(isPrime(quotient))	break;

		}
		else { // if number is not divisible by factor 
			// we need to choose next prime. search it from next of current factor
			for(factor = factor+1; !isPrime(factor) /* run until a prime is found */; 
				factor++){
			}
		}
	}
	//after while loop, the remaining quotient is also a prime,
	// so store it. (i.e print this until now)
	console.log(quotient);

};

printPrimeFactors(13195);
//printPrimeFactors(600851475143);