function isPalindrome(num){

	var test = num.toString().split('').reverse().join('');
	if( num === parseInt(test) ){
		return true
	}
	return false;
}

var max = 0;
var largestX = 0;
var largestY = 0;

for(var x=100; x<1000; x++){
	// we don't need to start with y=100, since we already computed that combination
	for(var y=x; y<1000; y++){
		product = x * y;
		if(isPalindrome(product)){
			if(product > max){
				max = product;
				largestX = x;
				largestY = y;
			}
		}
	}
}

console.log("Max: " + max + " X:" + largestX + " Y:" + largestY );