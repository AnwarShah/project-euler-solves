/* problem url: https://projecteuler.net/problem=2 */
function fibEvenSum(){
	var f1 = 1;
	var f2 = 2;
	var f3;
	var sum = 0;
	sum += f2; 	// add the first even fibonacci
	while(true){	// an infinite loop. though we will eventually be out of it using break; 
		f3 = f2 + f1;
		if(f3 > 4000000) { 	// if current fibonacci is greater that 4000000, get out of the loop
			break;
		}
		if(f3%2 === 0) { // if even
			sum+=f3;		
		}
		//re-assign
		f1 = f2;	// current f2 will be f1 next round
		f2 = f3;	// and similarly f3 will be f2
	}

	document.getElementById("result").innerHTML = sum;
}
