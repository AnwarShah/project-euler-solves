=begin
	This program takes a number as an input and returns sum of all of it's digits 
=end

def digitSum(num)
	sum = 0	#sum is zero initially
=begin 
	we will divide the number by 10 until number becomes zero
	and add those quotient and last remainder
=end
	quotient = num #initially its is equal to num
	while quotient >= 10
		sum += quotient % 10
		quotient = quotient/10	# reduce the number
	end
	#when number is less than 10, add the remainder
	sum += quotient
	return sum
end

puts digitSum 12367
