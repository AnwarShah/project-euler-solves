=begin This program prompts user to input a line of text and some words to hide from it
and then print the line redacting those word. Replacing them with "REDACTED" word
=end

#issue#1, currently this only works with words that separted with whitespace character 

puts "Enter a line of text containing sensitive words"
text = gets.chomp

puts "Enter the word(s) to hide"
redact = gets.chomp
redact.downcase! #make it lowercase

words = text.split(" ")

words.each do |word|
    if redact.include?(word) 
        print "REDACTED "
    else
        print word + " "
    end
end