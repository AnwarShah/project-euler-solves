sum = 0
10.upto(100) do |i|
	sum += i
end

puts "The sum of all integers from 10 to 100 (inclusive) is " + sum.to_s
