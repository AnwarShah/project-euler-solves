def isPrime?(num)
	return true if num == 2 or num==3
	return false if num%2 == 0 or num%3 == 0
	i = 5 
	w = 2	
	while i*i <= num do 
		return false if num%i == 0
		i=i+w
		w = 6-w
	end
	return true	#return true otherwise
end

def prime_factor(num)
	factor = 2	#first factor is 2
	while true	#loop until break 
		if num%factor==0 
			num = num/factor
			break if isPrime?num #if num becomes prime, our job done
		else	#num is not divisible by factor. time to change it
			factor = factor+1
			while true 
				break if isPrime?factor
				factor += 1
			end
		end	#end if-else
	end #end outer while
	num 	#return the largest prime factor, it is num!
end

# puts isPrime?(377)
puts prime_factor(13195)