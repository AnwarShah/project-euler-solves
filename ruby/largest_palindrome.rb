#Find the largest palindrome of product of two three-digit numbers
# https://projecteuler.net/problem=4
def is_palindrome?(num)
	num = num.to_s	#convert to string
	rev_num = num.reverse
	return true if rev_num == num
	return false #otherwise
end

max = largest_X  = largest_Y = 0
100.upto(999) {
	|x|
	x.upto(999){
		|y|
		product = x * y
		if is_palindrome?(product) && product > max
			max = product
			largest_X = x
			largest_Y = y
		end
	}
}
printf("Largest palindrome is: %d, X: %d, Y: %d\n", max, largest_X, largest_Y)