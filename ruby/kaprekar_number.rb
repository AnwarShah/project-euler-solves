""" Problem source: https://rubymonk.com/learning/books/1-ruby-primer/problems/150-kaprekar-s-number
9 is a Kaprekar number since 
9 ^ 2 = 81 and 8 + 1 = 9

297 is also Kaprekar number since 
297 ^ 2 = 88209 and 88 + 209 = 297.

In short, for a Kaprekar number k with n-digits, if you square it and 
add the right n digits to the left n or n-1 digits, 
the resultant sum is k. 
Find if a given number is a Kaprekar number."""
 
def kaprekar?(k)
  digit_count = k.to_s.length
  square = k**2
  num_string = square.to_s
  # slice the string from -digit_count index upto digit_count length
  right_part = num_string.slice(-digit_count, digit_count)
  left_part = num_string.slice(0, num_string.length - digit_count)
  
  if left_part.to_i + right_part.to_i == k
    return true
  else
    return false
  end
end

puts kaprekar?(297)
