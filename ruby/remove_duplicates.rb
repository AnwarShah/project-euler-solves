def removeDuplicates(lst)
	retList = []	#list to be returned
	lst.each {
		|x|
		if !retList.include?(x)	#if retList doesn't have the item
			retList.push(x)	#add it
		end
	}
	return retList #return the new list
end 

a = [1, 2, 1, 1, 2122, 12]
a= removeDuplicates(a)
print a.to_s + "\n"