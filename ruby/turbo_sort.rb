#This method exchange two elements in an array
def exchange(a, i , j)
	temp = a[i]
	a[i] = a[j]
	a[j] = temp
end

def quickSort(a, p, r)
	if p < r
		q = partition(a, p, r)
		quickSort(a, p, q-1)
		quickSort(a, q+1, r)
	end
end

def partition(a, low, high)
	pivot = a[high]
	i = low-1
	for j in low...high	#Note: Last index excluded
		if a[j] <= pivot
			i = i+1
			exchange(a, i, j)
		end
	end
	exchange(a, i+1, high)
	return i+1
end


lines = gets.chomp.to_i
arr = []

lines.times do 
	arr << gets.chomp.to_i
end

quickSort(arr, 0, lines-1)
puts arr