#without using 'Prime' class
# Finding 10001th prime

def isPrime?(num)
	return false if num < 1
	2.upto(Math.sqrt(num)) { |x|
		return false if num%x == 0
	}
	return true	
end

counter = 0
num = 2
while true
	counter += 1 if isPrime? num
	break if counter == 10001
	num += 1
end
puts num
