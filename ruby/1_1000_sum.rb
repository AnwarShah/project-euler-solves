# sum of all integers from 1 to 1000

sum = 0
count = 100
count.times do
	sum += count
	count -= 1
end

puts "The sum of all integers from 1 to 1000 is " + sum.to_s
