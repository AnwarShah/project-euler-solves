=begin
	This program takes a line of text and prints the occurence of each symbols
	with sorted occurence count. 
=end

puts "Enter a line of text"
text = gets.chomp

frequencies = Hash.new(0)

for i in 0...text.length #Note: index of text.length excluded
	frequencies[text[i]] += 1 
end

frequencies = frequencies.sort_by { 
	|symbol , count|
    count
}
frequencies.reverse! #reversing to get highest first

frequencies.each {
    |item, count|
    puts "#{item} => #{count} "
}