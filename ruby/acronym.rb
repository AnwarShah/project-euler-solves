=begin
	This program reads an String and returns it's acronym.
	For example, "Internation Business Machine" would return "IBM"	
=end

def acroynym(str)
	str = str.split()	#split the string into words
	acro = ""	#acronym. initialize with empty string
	for word in str
		acro += word[0]	#add the first character of each word
	end
	return acro
end

puts acroynym "International Business Machine" #call the function 
puts acroynym "Table of Content"