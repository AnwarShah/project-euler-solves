# Euler problem 6: https://projecteuler.net/problem=6
# Find the difference between the sum of the squares of the first one 
# hundred natural numbers and the square of the sum.

sum, sum_of_squares = 0, 0

1.upto(100) { |x| 
	sum += x
	sum_of_squares += x**2
}

puts (sum**2) - sum_of_squares