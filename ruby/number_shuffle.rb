# source: https://rubymonk.com/learning/books/1-ruby-primer/problems/154-permutations
# Problem Statement
# Given a 3 or 4 digit number with distinct digits, 
# return a sorted array of all the unique numbers 
# that can be formed with those digits.

#Example: 
#Given: 123 
#Return: [123, 132, 213, 231, 312, 321]

def number_shuffle(number)
  numbers = number.to_s.split('')
  numbers = numbers.collect { |x| x.to_i}
  uniq_nums = numbers.permutation.to_a
  uniq_nums = uniq_nums.collect { |x| x.join }
  uniq_nums = uniq_nums.collect { |x| x.to_i }
  return uniq_nums
end

p number_shuffle(123)
