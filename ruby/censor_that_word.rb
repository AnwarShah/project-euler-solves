=begin
	This program takes a string and a word and replace the word with * (asterisk).
	The number of asterisk is equal to the length of the word 
=end

def censor(str, word)
	if str.include?(word)
		idx = str.index(word)	#find the index of the matched word
		# then replace characters one by one with *
		# starting from index 'idx' and with length of 'word' times
		for i in idx...(idx+word.length)	#exclusive loop
			str[i] = '*'
		end
	end
end

str = "This is our country, Bangladesh"

censor(str, 'our')
puts str