require 'prime'

def primeFactor(num)
	pr = Prime.instance
	until pr.prime?num	#until num becomes itself a prime
		pr.each() {
			|x|
			factor = x
			if num%factor == 0
				num = num/factor
				break if pr.prime?num
			end
		}
	end
	return num
end

puts primeFactor(13195)
