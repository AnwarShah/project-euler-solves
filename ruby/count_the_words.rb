puts "Enter a line of text"
text = gets.chomp

words = text.split()
frequencies = Hash.new(0)

words.each { 
	|word| 
	frequencies[word] += 1 
}

frequencies = frequencies.sort_by { 
	|word , count|
    count
}
frequencies.reverse! #reversing to get highest first

frequencies.each {
    |item, count|
    puts "#{item} #{count}"
}