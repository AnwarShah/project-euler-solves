#problem source: http://www.codechef.com/problems/HOLES
holes = {
	"A" => 1,
	"B" => 2,
	"D" => 1,
	"O" => 1,
	"P" => 1,
	"Q" => 1,
	"R" => 1
}

lines =  gets.chomp.to_i # number of test
sums = []

lines.times do 
	line = gets.chomp
	sums << line.split(//).inject(0) { |sum, x| 
		sum += holes.include?(x) ? holes[x] : 0
	}
end

puts sums

