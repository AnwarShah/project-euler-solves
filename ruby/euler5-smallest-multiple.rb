# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20? 
# https://projecteuler.net/problem=5 
# Answer is 232 792 560 

def divisible_by_all?(num) 
  #returns true if the number num is divisible by all numbers from 1 upto LIMIT
	1.upto(20){ |x|
      return false if num%x != 0
	}
	return true	#not returned false? then it is true
end

#loop from 20 to upward until we find the smallest divisible
num = 2520
while true
  if divisible_by_all?(num) 
    break
  end
  num = num+20 #increment for next loop
end

puts num
