def factorial(n)
	(1..n).reduce(:*)
end

lines = gets.chomp.to_i # number of test cases
factorials = [] # store results

lines.times do 
	num = gets.chomp.to_i
	factorials << factorial(num)
end

puts factorials