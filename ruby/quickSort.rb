#This method exchange two elements in an array
def exchange(a, i , j)
	temp = a[i]
	a[i] = a[j]
	a[j] = temp
end

=begin 
	This method takes an array of numbers sort that array in place.
	The parameters are a (the array), p (the lower index), r (the highest index)
=end
def quickSort(a, p, r)
	if p < r
		q = partition(a, p, r)
		quickSort(a, p, q-1)
		quickSort(a, q+1, r)
	end
end

=begin
	This method do the partitioning in QuickSort.
	Parameters are a (the array), low (lower index of the partition), high (highest index)
=end
def partition(a, low, high)
	pivot = a[high]
	i = low-1
	for j in low...high	#Note: Last index excluded
		if a[j] <= pivot
			i = i+1
			exchange(a, i, j)
		end
	end
	exchange(a, i+1, high)
	return i+1
end


a = [3, 4, 12, 5, 67, 32, 67, 3, 21, 567, 21]

quickSort(a, 0, a.length-1)

puts a