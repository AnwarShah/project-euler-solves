=begin 
	This program takes a number and prints it's factorial in console.
=end

def calcFactorial(num)
	product = 1
	count = num
	
	count.times do	#loop 'num' number of times
		product *= count
		count -= 1	#decrease by 1
	end
	return product
end

print "Type a number: "
num = gets.to_i
fact = calcFactorial(num)

puts num.to_s + "! = " + fact.to_s 
