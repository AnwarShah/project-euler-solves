=begin
Problem source: https://projecteuler.net/problem=2.
Find the sum of even-valued fibonacci numbers that do not exceed four million
=end

f1 = 1	#first fibonacci number 
f2 = 2	#second fibonacci number
f3 = f1 + f2
sum = f2	# add the first even sum

until f3 > 4000000	#until f3 is greater than 4 million 
	if f3%2 == 0
		sum += f3
	end
	# update f1 and f2
	f1 = f2
	f2 = f3
	f3 = f2 + f1
end

puts sum