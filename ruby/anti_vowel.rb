=begin 
	This program removes vowel from text
=end

def isVowel(ch)
=begin 
	This function that checks whether a character ch is a vowel. 
	Returns true if it is, otherwise false
=end
	ch.downcase!	#convert to lower case
	vowels = ['a', 'e', 'i', 'o' , 'u'] 
	if vowels.include?(ch)	#if ch is a vowel 
		return true
	else
		return false
	end
end

def antiVowel(str)
=begin 
	This function takes a string 'str' and 
	returns another string without vowels
=end
	retStr = "" #string that would be returned
	#iterate through the 'str' string
	str.each_char {
		| ch |
		if !isVowel(ch) 	#if ch is not a vowel
			retStr += ch 	#add the return string
		end
	}
	return retStr
end

puts antiVowel("This is a text")
