public class EvenSum{
	/**
	*	This function computes the sum of fibonacci numbers
	*	which are not larger than 4, 000, 000 and 
	* 	divisible by 2, that is even.
	*/
	public void compute(){
		long f1 = 1;
		long f2 = 2;	// this is divisible
		long f3 = f1 + f2; // current fibonacci
		long sum = 0;
		sum += f2;	// add the first even number
		for(int i=3; i<=1000000; i++){	// though the loop will never run that times!
			f3 = f2 + f1;
			// if the current fibonacci number is greater that 4 million, break out of the loop
			if(f3 > 4000000) break;		
			if(f3%2 == 0){	// if even
				sum+= f3;	// add the the sum
			}

			f1 = f2; // current f2 will be f1 in the next round
			f2 = f3; // current f3 will be f2 in the next round
		}
		System.out.println(sum);
	}
}

class EvenSumPrinter{
	public static void main(String[] args){
		EvenSum es = new EvenSum();
		es.compute();
	}
}
