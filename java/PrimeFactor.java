/*This program will return the largest prime factor of a number */

public class PrimeFactor{
 /**
     * Determines whether a number num is prime
     * @param num an integer
     * @return boolean, true if num is prime, false otherwise
     */
     public boolean isPrime(int num)
    {
        if (num==2) return true;
        if (num==3) return true;
        if (num%2 == 0) return false;
        if (num%3 == 0 ) return false;
        
        int i=5, w=2;
        while (i*i <= num){
            if( num%i == 0){
                return false;
            }
            i = i+w;
            w = 6-w;
        }
        return true;   
    }
    
}

class driver{
	public static void main(String[] args){
		PrimeFactor pf = new PrimeFactor();
		System.out.println("Is prime: " + pf.isPrime(5));
	}
}
