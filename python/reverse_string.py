def reverse(text):
    index = len(text) - 1
    revText = ""
    #revText = [] #another way    
    while( index >= 0 ):
        revText += text[index]
        #revText.append(text[index]) #another way
        index=index-1
    
    #return "".join(revText)	#join with empty string another way
    return revText
    
print "To stop the program, press control+z key combo"    
while(True):
	text = raw_input("Enter a string to reverse: ")
	print reverse(text)