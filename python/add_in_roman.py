""" source: http://sites.fas.harvard.edu/~qr20/assignments/assign5.html"""
r_symbols = {
	"M" : 1000,
	"D" : 500,
	"C" : 100,
	"L" : 50,
	"X" : 10,
	"V" : 5,
	"I" : 1
}

def toDecimal(roman_str):
	"""This function takes a number in Roman literal and 
	return that number in decimal equivalent""" 
	# make a list from roman string
	str_list = list(roman_str)
	decimal_number = 0
	# loop through and add the respective value
	for i in str_list:
		decimal_number += r_symbols[i]
	return decimal_number

def toRoman(normal_str):
	"""This function takes a number in normal literal and 
	return that number in Roman equivalent"""
	
	roman_number_str = ""
	#convert the normal string into numbers 
	decimal_number = int(normal_str)

	# divide by highest roman literal M, then go down
	no_of_M = decimal_number / 1000	#no of M will be as such
	#the remainder after dividing will be the new number
	decimal_number = decimal_number%1000 

	#similarly for D
	no_of_D = decimal_number / 500 
	decimal_number = decimal_number%500

	#similarly for C
	no_of_C = decimal_number / 100 
	decimal_number = decimal_number%100

	#similarly for L
	no_of_L = decimal_number / 50 
	decimal_number = decimal_number%50
	
	#similarly for X
	no_of_X = decimal_number / 10 
	decimal_number = decimal_number%10

	#similarly for V
	no_of_V = decimal_number / 5 
	decimal_number = decimal_number%5

	#similarly for I
	no_of_I = decimal_number / 1 
	decimal_number = decimal_number%1

	#Now build up the string
	return no_of_M * 'M' + no_of_D * 'D' + no_of_C * 'C' + no_of_L * 'L' +\
	no_of_X * 'X' + no_of_V * 'V' + no_of_I * 'I'


def add(roman1, roman2):
	"""This function will add two number given in roman literal
	and return there sum"""

	num1 = toDecimal(roman1)
	num2 = toDecimal(roman2)
	result = num1 + num2

	return toRoman(result)

print add("CXXVII", "LXVII")		

	

	

