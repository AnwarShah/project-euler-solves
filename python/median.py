def median(lst):
	lst.sort()
	length = len(lst)
	medianIndex = [] 
	if length%2 != 0:
		medianIndex.append( (length/2 + 1)-1 ) # -1 for index
	else:
		medianIndex.append( (length/2-1) )
		medianIndex.append( (length/2+1-1))
	sum = 0.0
	for i in range(len(medianIndex)):
		sum+= lst[medianIndex[i]]
	
	return float(sum/len(medianIndex))

print median([7,3,1,4,5])