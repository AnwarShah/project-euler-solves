"""Problem source: http://sites.fas.harvard.edu/~qr20/assignments/assign2.html """

def acronym(text):
	"""This function receives a string and returns it's acronym.
	For example, if "International Business Machines" is given, it will
	return "IBM"""

	acro = "" #this will hold the value that will be returned
	# we are converting the string into list to be able
	# to access the first element of each word easily
	toList = text.split() 
	#looping through the word list, and adding first element of each 
	# word
	for word in toList:
		acro += word[0]
	# returning the result, i. e acro
	return acro

print acronym("International Business Machines")