def isPalindrome(text):
	if text[::-1] == text:
		return True
	else:
		return False


max = 0
largestX = 0
largestY = 0

for x in range(100, 1000):
	for y in range(x, 1000):
		product = x * y
		if isPalindrome(str(product)):
			if product > max:
				max = product
				largestY = y
				largestX = x


print "largest Palindrome is %d, largestX: %d, largestY: %d " %  (max, largestX,\
	largestY )