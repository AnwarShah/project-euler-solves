"""This function takes a string a returns it's value
using English alphabetic ordering. Lower and Upper case has 
equal value and Space and all other char has 0 value.
For example, a's value is 1, b is 2 
so on"""

import string
#Create the dictionary
#first create ascii lowercase letters
chars = list(string.ascii_lowercase)
vals = range(1, 27) #number 1-26
letter_vals = dict(zip(chars,vals))

def textToVal(text):
	num = 0
	for i in text:
		#convert to lowercase
		i = i.lower()
		#if a non ascii char is found, skip
		if i not in chars:
			continue
		num += letter_vals[i] 
	return num

print textToVal("Mohammad Anwar Shah")