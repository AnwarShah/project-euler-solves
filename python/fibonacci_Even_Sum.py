"""https://projecteuler.net/problem=2.
Find the sum of even-valued fibonacci numbers that do not exceed four million"""

f1 = 1
f2 = 2
f3 = f2 + f1

sum = f2 #adding the first even-valued fibo

while True: #loop will continue until 4 million exceed
	if f3 > 4000000:
		break
	f3 = f2 + f1
	if( f3%2 == 0 ):
		sum+=f3
	# now update f2 and f1
	f1 = f2	#first f1
	f2 = f3 

print sum
	
