"""
def remove_duplicates(lst):
	my_dict = {}
	my_list = []
	# first store them in dict
	# duplicates would be stored in there respective key again and again.
	for item in lst:
		my_dict[item] = item
	#then copy them to list
	for item in my_dict:
		my_list.append(item)
	
	return my_list
"""
def remove_duplicates(lst):
    my_list = []
    for item in lst:
        if item not in my_list:
            my_list.append(item)
    
    return my_list
    
print remove_duplicates([1,2,1,1,2])