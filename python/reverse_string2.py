text = "This is a text"

print text[::-1]

text2 = "This is text2"

revText2 = ""
for i in reversed(range(len(text2))):
	revText2 += text2[i]

print revText2

#another one
text3 = "This is text3"
revText3 = ""
for i in range(len(text3)-1, -1, -1): 
	#the indexing starts from len(text3)-1 i.e rightmost index
	# to the 0 (1 step ahead of -1) and will do in reverse
	revText3 += text3[i]

print revText3
