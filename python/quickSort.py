def quick_sort(A, p, r):
	if p < r:
		q = partition(A, p, r)
		quick_sort(A, p, q-1);
		quick_sort(A, q+1, r)

def partition(A, p, r):
	pivot = A[r]	# right-most is the pivot
	i = p - 1
	for j in range(p, r) :
		if A[j] <= pivot:
			i=i+1
			exchange(A, i, j)
	exchange(A, i+1, r)
	return i+1

def exchange(A, i, j):
	"""Exchange two elements of a list"""
	temp = A[j]
	A[j] = A[i]
	A[i] = temp

lst = [3, 4, 4, 2, 1, 5, 7, 8, 4, 32, 1, 12, 9403, 12,1 ,2, 12, 12 ,535,45, 37,32,78,234]

quick_sort(lst, 0, len(lst)-1)

print lst