def is_prime(num):
    if num == 2:
        return True
    if num == 3:
        return True
    if num % 2 == 0:
        return False
    if num % 3 == 0:
        return False
    i = 5
    w = 2
    while i * i <= num:
        if num % i == 0:
            return False
        i += w
        w = 6 - w
    return True

def summation_of_prime():
  sum = 0
  for i in range(2, 2000000):
    if is_prime(i):
      sum += i
  return sum

if __name__ == '__main__':
    print(summation_of_prime())