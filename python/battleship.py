from random import randint

board = []

for x in range(5):
    board.append(["O"] * 5)

def print_board(board):
    for row in board:
        print " ".join(row)

print "Let's play Battleship!"
print_board(board)

def random_row(board):
    return randint(0, len(board) - 1)

def random_col(board):
    return randint(0, len(board[0]) - 1)

ship_row = random_row(board)
ship_col = random_col(board)
# print the location
#print ship_row
#print ship_col

#looping for 4 turns
for turn in range(4):
    print "Turn", turn + 1
    # adding 1 will make the zero indexing transparent to the user
    guess_row = int(raw_input("Guess Row:")) - 1
    guess_col = int(raw_input("Guess Col:")) - 1
    if guess_row == ship_row and guess_col == ship_col:
        print "Congratulations! You sunk my battleship!"
        break;
    else:   #if misses, what to do?
        if (guess_row < 0 or guess_row > 4) or (guess_col < 0 or guess_col > 4):   #if outside
            print "Oops, that's not even in the ocean."
        elif(board[guess_row][guess_col] == "X"): #guessed already?
            print "You guessed that one already."
        else: #or just missed
            print "You missed my battleship!"
            board[guess_row][guess_col] = "X"
            if( turn == 3 ):
                print "Game Over"
                break;
        # Print (turn + 1) here!
        print_board(board)
        if( turn == 3 ):
            print "Game Over"
