"""This program takes a number and returns the sum of all of it's digits"""

def digit_sum(n):
    sum = 0	#sum is zero initially
    quotient = n; # qoutient holds the result in each division step

    while( quotient > 0 ):	#until quotient becomes zero
        sum += quotient%10  # first add remainder for this round
        quotient = quotient/10  # do the actual division

    # at last, quotient will be single digit, we should just add
    # that as last remainder
    sum+= quotient

    return sum

print digit_sum(1234)
