"""Finding the longest common substring
	source: http://sites.fas.harvard.edu/~qr20/assignments/assign3.html"""

str1 = "abcdefghijk"
str2 = "zzjskasbcdjkm"

def longestSubSequence(str1, str2):

	length_dict = {} #Dict to contain common string and length

	#we need to start the loop from from various position, such as 0, 1, 2 ..
	for i in range(len(str1)):
		outerStr = str1[i:]	#starting with i index to the last
		#we need length of 1 to length(str). range() does not generate end index
		for l in range(1, len(outerStr)+1):
			candStr = outerStr[:l]
			if candStr in str2:
				length_dict[candStr] = l #key is string, value is the length

	#print length_dict

	#Now find the longest string by searching in Dict value
	# get a list of all the length
	length_list = list(length_dict.values())
	string_list = list(length_dict.keys())

	longest_length_index = length_list.index(max(length_list))

	return string_list[longest_length_index]

print longestSubSequence(str1, str2)