"""This program eats vowel from a line of text"""
def anti_vowel(text):
    nText = ""
    for i in text:
    	if( isVowel(i) == True ):
    		nText = nText
    	else:
    		nText += i
    return nText


def isVowel(char):
    if char.lower() == 'a' or char.lower() == 'e' or char.lower() == 'i' or char.lower() == 'o' or char.lower() == 'u':
        return True
    else: 
    	return False

print anti_vowel("This is a text")
