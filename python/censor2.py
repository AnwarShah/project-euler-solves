"""This program takes a string and a word and replace that word with 
asterisk. The number of asterisk is equal to the length of the word"""

def censor(text, word):
    wordlen = len(word)
    return text.replace(word, '*' * wordlen)

print censor("This is aa text text", "text")
