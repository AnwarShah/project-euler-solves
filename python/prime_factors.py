__author__ = 'mas'
"""This problem will determine the largest prime factor of the number
13195. For 10, two prime factors are 2, 5, because 2x5=10"""

"""This function determines whether a number num is prime or not.
    If prime, it returns true. Otherwise, false"""


def isPrime(num):
    if num == 2:
        return True
    if num == 3:
        return True
    if num % 2 == 0:
        return False
    if num % 3 == 0:
        return False
    i = 5
    w = 2
    while i * i <= num:
        if num % i == 0:
            return False
        i += w
        w = 6 - w
    return True


"""This function returns the largest factor of a number which is prime"""


def largestPrimeFactor(num):
    # first quotient will be the number itself
    quotient = num
    factor = 2  # first prime factor is 2.

    while True:  # loop until quotient is not itself prime
        if quotient % factor == 0:  # if divisible by current factor
            quotient = quotient / factor  #divide it and store quotient
            #if quotient itself becomes prime, that means
            # we are at the end of our job. break
            if isPrime(quotient) == True:
                break
        # if quotient can't be divided by current factor
        else:
            # next factor is factor+1
            factor = factor + 1
            #search the next prime factor
            while not isPrime(factor):
                factor = factor + 1
    print(quotient)


print(largestPrimeFactor(13195))
