import random

number = random.randrange(0, 100)
running = True
print "Welcome to Number guessing game! \nI have choosen a number. Can you\
 guess it?"
guess_count = 0

while running:
	guess = int(raw_input('Enter your guess: '))
	guess_count+=1
	if guess == number:
		print "Congrats! You correctly guessed the number in %d guesses" % (guess_count)
		running = False
	elif guess < number:
		print "Oops! The number is higher than that! Try again!"
	else: 
		print "Oops! The number is less than that! Try again!"

else:
	print 'The while loop is over.'

print 'Done'